'use strict';
module.exports = (sequelize, DataTypes) => {
    const Expense = sequelize.define('Expense', {
        moneySpent: DataTypes.DECIMAL,
        describe: DataTypes.STRING
    }, {});
    Expense.associate = function(models) {
        // associations can be defined here
        Expense.belongsTo(models.User);
    };
    return Expense;
};