'use strict';
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: DataTypes.STRING,
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {});
    User.associate = function(models) {
        // associations can be defined here
        User.hasMany(models.Expense);
    };
    return User;
};